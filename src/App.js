import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FeeCounter from './FeeCounter';

class App extends Component {
  render() {
    return (
      <div>
        <FeeCounter min={1} max={10} direction={''} currentValue={1} />
      </div>
    );
  } 
}

export default App;
